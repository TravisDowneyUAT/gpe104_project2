﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed; //speed the player will rotate
    public float forwardForce; //speed the player will move forward

    public GameObject bullet; //reference to the bullet prefab

    private Transform tf; //refence to self transform
    private Rigidbody2D rb; //reference to self rigidbody

    // Start is called before the first frame update
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //if the player presses the right arrow key or the D key, rotate the ship clockwise
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            tf.Rotate(-Vector3.forward * turnSpeed);
        }
        //if the player presses the left arrow key or the A key, rotate the ship counter-clockwise
        if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            tf.Rotate(Vector3.forward * turnSpeed);
        }
        //if the player presses the up arrow or the W key, move the player forward the in the direction they're facing
        if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            tf.position += tf.up * forwardForce * Time.deltaTime;
        }
        //if the player presses the spacebar, fire a bullet
        if (Input.GetKeyDown(KeyCode.Space))
        {
            FireProjectile();
        }
    }

    void FireProjectile()
    {
        //spawn a bullet at the player's position and rotation
        Instantiate(bullet, new Vector3(tf.position.x, tf.position.y, 0), tf.rotation);
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        //if the player is hit by an asteroid or an alien ship, reset the player's transform and velocity to zero
        //and decrease the player's lives
        if (col.gameObject.tag == "Asteroid" || col.gameObject.tag == "Alien")
        {
            rb.velocity = Vector3.zero;

            tf.position = Vector3.zero;

            DecreaseLives();
        }
    }

    public void DecreaseLives()
    {
       GameManager.manager.playerLives--; //decrease the player's lives by one
       GameManager.manager.playerLivesText.text = "Lives: " + GameManager.manager.playerLives; //update the lives in the UI

        //if the player's lives ever drop to zero, exit the game
        if (GameManager.manager.playerLives == 0)
        {
            Application.Quit();
            Debug.Log("Game Over!");
        }
        //else destroy any active targets and reset the player to center of the screen
        else
        {
            GameManager.manager.DestroyActiveTargets();
            rb.velocity = Vector3.zero;
            tf.position = Vector3.zero;
        }
    }

    //if the player leaves the camera view, decrease the player's lives
    public void OnBecameInvisible()
    {
        DecreaseLives();
    }


}
