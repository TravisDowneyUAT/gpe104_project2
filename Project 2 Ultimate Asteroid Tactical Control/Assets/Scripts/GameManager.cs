﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager manager; //singleton instance

    public GameObject[] asteroidSpawners; //array of asteroid spawn points

    public GameObject playerPrefab; //player prefab

    private int playerScore; //int to hold the players score
    private int playerHiScore; //int to hold the players hi score
    public int playerLives; //int to hold the players lives

    public List<GameObject> targets; //list to hold the target prefabs

    public List<GameObject> activeTargets; //list to hold the active targets
    [HideInInspector]
    public bool removeTargets; //bool to remove the targets from the Active Targets list
    [HideInInspector]
    public int maxActiveTargets = 3; //maximum number of active targets

    public Text playerScoreText; //reference to the score text in the UI
    public Text playerLivesText; //reference to the lives text in the UI
    public Text playerHiScoreText; //reference to the hi score text in the UI

    //Singleton Pattern
    private void Awake()
    {
        if (manager == null)
        {
            manager = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        playerHiScore = PlayerPrefs.GetInt("HiScore", 0); //setting the player's hi score

        GameStart(); //starting the game
    }

    void Update()
    {
        SpawnTargets(); //spawn the targets
    }

    void GameStart()
    {
        //spawn the player in the center of the screen
        Instantiate(playerPrefab, Vector3.zero, Quaternion.Euler(0, 0, 0));

        playerScore = 0; //set the player's score equal to zero
        playerLives = 3; //set the player's lives equal to three

        playerScoreText.text = "Score: " + playerScore; //set the text equal the player's score variable
        playerLivesText.text = "Lives: " + playerLives; //set the text equal to the player's lives variable
        playerHiScoreText.text = "HiScore: " + playerHiScore; //set the text equal to the player's hi score variable
    }

    void SpawnTargets()
    {
        //if the amount of active targets is less than the number of maximum active targets, spawn 3 targets
        if(activeTargets.Count < maxActiveTargets)
        {
            int id = Random.Range(0, asteroidSpawners.Length); //choose one of the spawn points from the array
            GameObject spawnPoint = asteroidSpawners[id]; //set the spawn point of the game object equal to the chosen spawn point from the array

            GameObject target = targets[Random.Range(0, targets.Count)]; //get a random target

            //spawn the given target at the given spawn point
            GameObject asteroidInstance = Instantiate(target, spawnPoint.transform.position, Quaternion.identity);
            GameObject enemyInstance = Instantiate(target, spawnPoint.transform.position, Quaternion.identity);
           
            //if the target is an asteroid, do the following
            if (gameObject.tag == "Asteroid")
            {
                Vector2 dir = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                dir.Normalize();
                asteroidInstance.GetComponent<AsteroidMovement>().dir = dir;
                
            }
            
            //if the target is an alien ship do the following
            if(gameObject.tag == "Alien")
            {
                Vector2 dir = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                dir.Normalize();
                enemyInstance.GetComponent<AlienAttack>().dir = dir;
                
            }
            
            //add the instance to the Active Targets list
            activeTargets.Add(asteroidInstance);
            activeTargets.Add(enemyInstance);  
        }
    }

    public void DestroyActiveTargets()
    {
        removeTargets = true; //set the remove targets bool to true
        activeTargets.ForEach(Destroy);
        activeTargets.Clear(); //clear the list
        removeTargets = false; //set the remove targets bool to false
    }

   
    public void IncreaseScore()
    {
        playerScore++; //increase the player's score by 1

        playerScoreText.text = "Score: " + playerScore; //push the new score to the text in the UI
        
        //if the player's current score is higher than the hi score, make the current sore the hi score
        if (playerScore > playerHiScore)
        {
            playerHiScore = playerScore;
            playerHiScoreText.text = "HiScore: " + playerHiScore;

            PlayerPrefs.SetInt("HiScore", playerHiScore);
        }
    }

    
}
