﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienAttack : MonoBehaviour
{
    private Transform tf; //reference to the game object's transform
    private Transform playerTF; //reference to the player's transform
    [HideInInspector]
    public Vector3 dir; //direction vector to move to the player
    public float rotationSpeed; //speed at which the game object will rotate
    public float force; //speed at which the game object will move

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        playerTF = GameObject.FindWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        //get the direction to move to by subtracting the alien ship's position and the player's position
        dir = playerTF.position - tf.position;
        dir.Normalize();
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        Quaternion targetRotation = Quaternion.Euler(0, 0, angle);
        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, rotationSpeed * Time.deltaTime);

        tf.Translate(Time.deltaTime * force, 0, 0);
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Bullet"))
        {
            GameManager.manager.activeTargets.Remove(gameObject); //remove the prefab from the Active Targets list
            GameManager.manager.IncreaseScore(); //increase the player's score
            Destroy(this.gameObject); //destroy the game object
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            GameManager.manager.activeTargets.Remove(gameObject);
            GameManager.manager.playerPrefab.GetComponent<PlayerMovement>().DecreaseLives();
            Destroy(this.gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

}
