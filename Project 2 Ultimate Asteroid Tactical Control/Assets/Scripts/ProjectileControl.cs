﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileControl : MonoBehaviour
{
    public float destroySeconds = 1.0f; //float holding the amount of time before the bullet is destroyed
    private Rigidbody2D rb; //reference to self rigidbody

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, destroySeconds); //destroy the game objects after a given number of seconds

        rb = GetComponent<Rigidbody2D>();
        rb.AddForce(transform.up * 350); //push the bullet by adding force to the bullet's rigidbody
    }
}
