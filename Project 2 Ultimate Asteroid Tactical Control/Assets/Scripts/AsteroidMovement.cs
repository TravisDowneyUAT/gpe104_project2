﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMovement : MonoBehaviour
{
    
    private Transform tf; //reference to the game object's transform
    private Transform playerTF; //reference to the player's transform
    [HideInInspector]
    public Vector2 dir; //direction vector to move to the player
    public float speed; //speed at which the asteroid will move

    // Start is called before the first frame update
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
        playerTF = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

        //get the direction to move to by subtracting the alien ship's position and the player's position
        dir = playerTF.position - tf.position;
        dir.Normalize();
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        tf.rotation = Quaternion.Euler(0.0f, 0.0f, angle);
    }

    // Update is called once per frame
    void Update()
    {
        tf.Translate(Time.deltaTime * speed, 0, 0); //move the asteroid based on the speed variable
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Bullet"))
        {
            GameManager.manager.activeTargets.Remove(gameObject); //remove the asteroid from the Active Targets list once it's destroyed
            GameManager.manager.IncreaseScore(); //increase the player's score
            Destroy(this.gameObject); //destroy the game object
        }
        else if (GameManager.manager.removeTargets == false)
        {
            if (this.gameObject == null)
            {
                GameManager.manager.activeTargets.Remove(this.gameObject); //remove the asteroid from the Active Targets list once it's destroyed
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            GameManager.manager.activeTargets.Remove(gameObject);
            GameManager.manager.playerPrefab.GetComponent<PlayerMovement>().DecreaseLives();
            Destroy(this.gameObject);
        }
    }

    //destroy the gameobject once it's off screen
    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
